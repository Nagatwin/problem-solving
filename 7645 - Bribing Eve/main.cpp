#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>

using namespace std;

struct vect2{
    int x, y;
    vect2(int x_, int y_) : x(x_), y(y_) {}

    inline double slope(vect2 const & other) const{
        int x_diff = (other.x - x);
        if (x_diff == 0)
            return std::numeric_limits<double>::infinity();

        int y_diff = (other.y - y);

        // Use -slope to work with positive values, easier with inf
        return -double(y_diff) / double(x_diff);
    }
};

int main(){
    int N;
    while (scanf("%d", &N) == 1){
        int best_rank = 1;

        // products with same rank
        int same_rank = 0;

        int x0, y0;
        scanf("%d %d", &x0, &y0);
        vect2 v0(x0, y0);

        int x, y;

        // slopes from upper-left quarter
        vector<double> ul_quarter;
        // slopes from bottom-right quarter
        vector<double> br_quarter;
        for (int i = 1; i < N; i++){
            scanf("%d %d", &x, &y);
            // Can't get better than this one
            if (x > x0 && y > y0){
                best_rank ++;
                continue;
            }
            
            // This one has the exact same rank
            if (x == x0 && y == y0){
                same_rank ++;
                continue;
            }

            // This one is strictly worse
            if (x < x0 && y < y0)
                continue;
            
            double slope = vect2(x, y).slope(v0);
            if (x >= x0 && y <= y0)
                // bottom-right
                br_quarter.push_back(slope);
            else
                // upper-left
                ul_quarter.push_back(slope);
        }

        // sort slopes
        sort(ul_quarter.begin(), ul_quarter.end());
        sort(br_quarter.begin(), br_quarter.end());

        // do the search from horizontal
        unsigned int br_index = 0;
        unsigned int ul_index = 0;

        // points ranked better in worst case
        unsigned int max_better_ranked = ul_quarter.size();
        for (unsigned int i = 0; i < br_quarter.size(); i++)
            if (br_quarter[i] == 0)
                max_better_ranked++;

        // points ranked better in best case
        unsigned int min_better_ranked = ul_quarter.size();
        for (unsigned int i = 0; i < ul_quarter.size(); i++)
            if (ul_quarter[i] == 0)
                min_better_ranked--;

        // Points that are ranked better at step i
        unsigned int better_ranked = ul_quarter.size();
        
        while (br_index != br_quarter.size() || ul_index != ul_quarter.size()){
            // Current slope value we analyze
            double val = std::numeric_limits<double>::infinity();
            if (br_index != br_quarter.size())
                val = br_quarter[br_index];
            if (ul_index != ul_quarter.size())
                val = min(val, ul_quarter[ul_index]);
            
            // Remove equal : this is the best case
            unsigned int better_ranked_worse = better_ranked;
            while (ul_index != ul_quarter.size() && ul_quarter[ul_index] == val){
                ul_index ++;
                better_ranked--;
            }
            min_better_ranked = min(min_better_ranked, better_ranked);

            // Add equal : this is the worst case
            while(br_index != br_quarter.size() && br_quarter[br_index] == val){
                br_index++;
                better_ranked_worse++;
                better_ranked++;
            }
            max_better_ranked = max(max_better_ranked, better_ranked_worse);
        }
        
        cout << min_better_ranked + best_rank << " " << max_better_ranked + best_rank + same_rank << endl;
    }
    return 0;
}