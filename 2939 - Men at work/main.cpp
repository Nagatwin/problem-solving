#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <string>

using namespace std;

// Cell struct
struct cell {
    bool obstacle_start;
    int period;

    cell() : obstacle_start(false), period(0) {}

    // check if cell is free at time t
    inline bool is_free(int const time) const {
        return ((period == 0) ? !obstacle_start : (time / period) % 2 == (obstacle_start ? 1 : 0));
    }
};

// State for bfs
struct state {
    int time;
    int length;
    int x;
    int y;

    state(int time_, int length_, int x_, int y_) : time(time_), length(length_), x(x_), y(y_) {}
};

// from https://codes-sources.commentcamarche.net/source/9638-ppcm-de-deux-nombres-tout-compilateur
inline int ppcm(int const X, int const Y)
{
    int A=X;
    int B=Y;
    while (A!=B) {
        while (A>B) B=B+Y;
        while (A<B) A=A+X;
    }
    return A;
}

// Taken & adapted from course slides
inline int BFS(state const root, int const N, vector<vector<cell>> const & cells, int const mod) {
    // Queue for bfs
    queue<state> Q;

    // bool table for already checked states
    vector<vector<vector<bool>>> checked(1, vector<vector<bool>>(N, vector<bool>(N, false)));
    checked[root.time][root.x][root.y] = true;
    
    Q.push(root);
    while (!Q.empty()) {
        // get first state
        state u = Q.front();
        Q.pop();
        
        // check if it is a solution
        if (u.x == N-1 && u.y == N-1)
            return u.length;

        // compute next time and length
        unsigned int next_time = (u.time+1)%mod;
        unsigned int next_length = u.length + 1;

        // Add a row to checked vector if we reached the max
        if (next_time >= checked.size())
            checked.push_back(vector<vector<bool>>(N, vector<bool>(N, false)));

        // generate surrounding states
        if (u.x > 0 && !checked[next_time][u.x-1][u.y]){
            checked[next_time][u.x-1][u.y] = true;
            if (cells[u.x-1][u.y].is_free(next_time))
                Q.push(state(next_time, next_length, u.x-1, u.y));
        }

        if (u.y > 0 && !checked[next_time][u.x][u.y-1]){
            checked[next_time][u.x][u.y-1] = true;
            if (cells[u.x][u.y-1].is_free(next_time))
            Q.push(state(next_time, next_length, u.x, u.y-1));
        }

        if (u.x < N-1 && !checked[next_time][u.x+1][u.y]){
            checked[next_time][u.x+1][u.y] = true;
            if(cells[u.x+1][u.y].is_free(next_time))
                Q.push(state(next_time, next_length, u.x+1, u.y));
        }

        if (u.y < N-1 && !checked[next_time][u.x][u.y+1]){
            checked[next_time][u.x][u.y+1] = true;
            if(cells[u.x][u.y+1].is_free(next_time))
                Q.push(state(next_time, next_length, u.x, u.y+1));
        }

        if (!checked[next_time][u.x][u.y]){
            checked[next_time][u.x][u.y] = true;
            if(cells[u.x][u.y].is_free(next_time))
                Q.push(state(next_time, next_length, u.x, u.y));
        }
    }
    return -1;
}

int main()
{
    bool f = true;
    // 2<<50
    int N;
    while (scanf("%d", &N) == 1){
        // (╯°□°）╯︵ ┻━┻
        if (!f)
            cout << endl;
        else
            f = false;
        
        // read cells
        vector<vector<cell>> cells(N, vector<cell>(N, cell()));
        
        string line;
        for (int i = 0; i < N; i++){
            cin >> line;
            for (int j = 0; j < N; j++)
                cells[i][j].obstacle_start = (line.at(j) == '*');
        }

        // for computation of ppcm
        vector<int> seen;
        for (int i = 0; i < N; i++){
            cin >> line;
            for (int j = 0; j < N; j++){
                int period = int(line.at(j)) - 48;
                cells[i][j].period = period;
                if (period != 0 && find(seen.begin(), seen.end(), period) == seen.end())
                    seen.push_back(period);
            }
        }

        // ppcm seen
        int factor = 1;
        for (int s : seen)
            factor = ppcm(factor, s);

        // Do a bfs ? :o
        int result = BFS(state(0,0,0,0), N, cells, 2 * factor);

        // give result
        if (result == -1)
            cout << "NO" << endl;
        else
            cout << result << endl;
    }
    return 0;
}