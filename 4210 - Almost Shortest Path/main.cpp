#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// List the ways from the source to the current according to the predictions
inline vector<vector<int>> list_ways(vector<vector<int>> const & preds, int const source, int const current){
    if (current == source){
        vector<vector<int>> v(1, vector<int>(1,source));
        return v;
    }

    vector<vector<int>> res;
    for (int i : preds[current])
        for (vector<int> computed : list_ways(preds, source, i)){
            computed.push_back(current);
            res.push_back(computed);
        }
    return res;
}

// Compute the shortest ways from source to dest
inline vector<vector<int>> ways_to(int const source, int const dest, vector<vector<int>> const & matrix){
    // Basically an upgraded dijkstra
    int const N = matrix.size();
    vector<int> dists(N, -1);
    vector<bool> seen(N, false);
    vector<vector<int>> preds(N, vector<int>(1, -1));
    dists[source] = 0;

    int current = source;
    while(current != dest){
        seen[current] = true;
        for (int dest = 0; dest < N; dest ++){
            // If there is an arc
            if (matrix[current][dest] != -1){
                // If the target have no ways
                if (dists[dest] == -1 
                    // Or we have a better choice
                    || dists[dest] > dists[current] + matrix[current][dest]){
                    dists[dest] = dists[current] + matrix[current][dest];
                    preds[dest] = vector<int>(1, current);
                // If we have a tie
                } else if (dists[dest] == dists[current] + matrix[current][dest]){
                    preds[dest].push_back(current);
                }
            }
        }

        // Now select unseen node with lowest distance
        current = -1;
        for (int node = 0; node < N; node++)
            if (!seen[node] && dists[node] != -1 && (current == -1 || dists[node] < dists[current] || (current == dest && dists[node] == dists[current])))
                current = node;
        
        // No way found :(
        if (current == -1){
            vector<vector<int>> s(1, vector<int>(1, -1));
            return s;
        }
    }

    return list_ways(preds, source, dest);
}

int main()
{
    // N number of points
    // M edges
    int N, M;
    scanf("%d %d", &N, &M);

    while (M != 0){
        // Starting and destination points
        int S, D;
        scanf("%d %d", &S, &D);

        vector<vector<int>> matrix(N, vector<int>(N, -1));

        for (int i = 0; i < M; i++){
            int s, d, w;
            scanf("%d %d %d", &s, &d, &w);
            matrix[s][d] = w;
        }

        // compute shortest paths
        vector<vector<int>> ways = ways_to(S, D, matrix);

        // remove edges
        for (vector<int> way : ways){
            if (way[0] != -1)
                for (unsigned int k = 0; k < way.size() - 1; k++)
                    matrix[way[k]][way[k+1]] = -1;
        }

        // compute shortest pathes
        ways = ways_to(S, D, matrix);
        
        if (ways[0][0] == -1)
            // No way found
            cout << -1 << endl;
        else{
            // compute distance
            int length = 0;
            for (unsigned int k = 0; k < ways[0].size() - 1; k++)
                length += matrix[ways[0][k]][ways[0][k+1]];
            cout << length << endl;
        }

        scanf("%d %d", &N, &M);
    }
    return 0;
}