#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <string>
#include <cstring>

using namespace std;
#define MAXN 100001
char p[10][MAXN];
int T[MAXN+1];

inline void KMP(const unsigned int np, const unsigned int current){
    T[0] = -1;
    int cnd = 0;
    for (unsigned int i = 1; i <= np; i++) {
        T[i] = cnd;
        while (cnd >= 0 && p[current][cnd] != p[current][i])
            cnd = T[cnd];
        cnd++;
    }
}


int main(){
    int n, s;

    while(scanf("%d %d", &n, &s) == 2){
        vector<pair<unsigned long long, int>> values;
        for (int i = 0; i < s; i++){
            scanf("%s", p[i]);
            const unsigned int np = strlen(p[i]);

            KMP(np, i);

            unsigned int prefix_length = np;
            unsigned long long val = 0;

            //vector<unsigned int> overlaps;
            do {
                prefix_length = T[prefix_length];
                if ((int)prefix_length >= 2 * (int)np - n || prefix_length == 0)
                    val |= (((unsigned long long)1)<<((unsigned long long)prefix_length));
                
            } while(prefix_length != 0);

            values.push_back(make_pair(val, i));
        }

        sort(values.begin(), values.end());
        
        for (auto & v : values)
            puts(p[v.second]);
    }
    // m translation letters, n pairs
    // 1<m<500
    // 1<n<50
    /*int m, n;
    while (scanf("%d %d", &m, &n) == 2){
        
    }*/
    return 0;
}