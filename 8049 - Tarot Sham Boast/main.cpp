#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;

// From course slides
#define MAXN 100001
char p[10][MAXN];
int T[MAXN+1];

inline void KMP(const unsigned int np, const unsigned int current){
    T[0] = -1;
    int cnd = 0;
    for (unsigned int i = 1; i <= np; i++) {
        T[i] = cnd;
        while (cnd >= 0 && p[current][cnd] != p[current][i])
            cnd = T[cnd];
        cnd++;
    }
}


int main(){
    // n length of the length
    // s patterns
    int n, s;

    while(scanf("%d %d", &n, &s) == 2){
        // Mapping to values & index
        vector<pair<vector<unsigned int>, int>> values;
        for (int i = 0; i < s; i++){
            scanf("%s", p[i]);

            const unsigned int np = strlen(p[i]);

            // Run algo
            KMP(np, i);

            // Read table to get the overlaps lengths
            unsigned int prefix_length = np;

            vector<unsigned int> overlaps;
            do {
                prefix_length = T[prefix_length];
                // Filter unfeasible overlaps
                if ((int)prefix_length >= 2 * int(np) - n || prefix_length == 0)
                    overlaps.push_back(prefix_length);
                
            } while(prefix_length != 0);

            values.push_back(make_pair(overlaps, i));
        }

        // Sort by values
        sort(values.begin(), values.end());
        
        for (auto const & v : values)
            puts(p[v.second]);
    }
    return 0;
}