#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// Base struct
struct vector_2 {
    int x;
    int y;

    vector_2() : x(0), y(0) {}
    vector_2 (int x_, int y_) : x(x_), y(y_) {}

    // Square euclidian dist
    inline int sq_dist_eucl (const vector_2 & other) const{
        return (x - other.x) * (x - other.x) + (y - other.y) * (y - other.y);
    }
};

struct subnet {
    vector<int> cities;
    int cost;

    subnet() : cities(), cost(0){
        // read cities & cost
        int cities_in_net;
        scanf("%d %d", &cities_in_net, &cost);
        for (int c = 0; c < cities_in_net; c++){
            int city_id;
            scanf("%d", &city_id);
            cities.push_back(city_id);
        }

        // sort cities (important for our handling of connect)
        sort(cities.begin(), cities.end());
    }
};

// Basically union/join, but optimized for unoriented graph
inline void connect(vector<unsigned int> & connex, unsigned int const & low, unsigned int const & high){
    unsigned int old_parent = connex[high];
    unsigned int new_parent = connex[low];
    for (unsigned int i = 1; i < connex.size(); i++)
        if (connex[i] == old_parent)
            connex[i] = new_parent;
}

// Adapted from course
inline unsigned int Kruskal(vector<unsigned int> & connex, vector<pair<unsigned int, pair<unsigned int, unsigned int>>> & Edges) {
    unsigned int cost = 0;

    for(pair<unsigned int, pair<unsigned int, unsigned int>> & edge : Edges) {
        unsigned int start = edge.second.first;
        unsigned int end = edge.second.second;
        // Check if they belong to the same connex component
        if (connex[start] != connex[end]) {
            // Update all parents
            connect(connex, connex[start], connex[end]);
            // Add cost
            cost += edge.first;
        }
    }

    return cost;
}

// Reset union-find
inline void reinit_vector(vector<unsigned int> & connex, unsigned int const n){
    connex.clear();
    for (unsigned int i = 0; i <= n; i++)
        connex.push_back(i);
}

// q subnets; n cities
inline unsigned int get_best_cost(unsigned int n, vector<subnet> & subnets, vector<pair<unsigned int, pair<unsigned int, unsigned int>>> & edges){
    unsigned int q = subnets.size();

    vector<unsigned int> connex;
    reinit_vector(connex, n);
    
    // Cost without buying any subnet
    unsigned int best_cost = Kruskal(connex, edges);
    
    // Basically exploring permutations over subnets
    for (int s = 1; s < (1 << q); s++) {
        unsigned int total_cost = 0;
        reinit_vector(connex, n);
        
        // Compute starting graph
        for (unsigned int i = 0; i < q; i++)
            if (s & (1 << i)){
                total_cost += subnets[i].cost;
                for (unsigned int c = 0; c < subnets[i].cities.size() - 1; c++)
                    connect(connex, subnets[i].cities[c], subnets[i].cities[c+1]);
            }
        
        // Avoid a few cases
        if (total_cost < best_cost){
            // Update cost
            total_cost += Kruskal(connex, edges);

            if (total_cost < best_cost)
                best_cost = total_cost;
        }
    }
    return best_cost;
}

int main()
{
    // Number of cases
    int cases;

    scanf("%d", &cases);

    for (int cas = 0; cas < cases; cas++){
        // n cities, q subnets
        int n, q;
        scanf("%d %d", &n, &q);

        // read subnets
        vector<subnet> subnets;
        for (int sub = 0; sub < q; sub++){
            subnets.emplace_back();
        }

        // Read cities, keeping 1 to avoid shifting all indexes
        vector<vector_2> cities(1, vector_2());

        for (int city = 0; city < n; city++) {
            int x, y;
            scanf("%d %d", &x, &y);
            cities.emplace_back(x, y);
        }

        // Build edges
        vector<pair<unsigned int, pair<unsigned int, unsigned int>>> edges;

        for (int start_city = 1; start_city <= n - 1; start_city++)
            for (int end_city = start_city + 1; end_city <= n; end_city++)
                edges.push_back(make_pair(cities[start_city].sq_dist_eucl(cities[end_city]), 
                    make_pair(start_city, end_city)));
    
        // Sort edges by weight
        sort(edges.begin(), edges.end());

        // Do the computation
        cout << get_best_cost(n, subnets, edges) << endl;
        
        // (╯°□°）╯︵ ┻━┻
        if (cas != cases - 1)
            cout << endl;
    }
    return 0;
}