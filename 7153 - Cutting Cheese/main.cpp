#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

// Const
const double PI = acos(-1);

struct hole{
    double pos;
    double radius;
    double volume;
};

// Computes a spherical cap
inline double spherical_cap(double R, double h){
    return PI * h * h * (3 * R - h) / 3;
}

inline double cheeze_amount(double const pos, vector<hole> const & holes){
    double total_vol = 100.0 * 100.0 * pos;
    for (hole const& hole : holes) {
        if (hole.pos - hole.radius < pos){
            // Intersection
            if (hole.pos + hole.radius < pos){
                // full hole contained
                total_vol -= hole.volume;
            }else{
                // part hole contained
                total_vol -= spherical_cap(hole.radius, pos - hole.pos + hole.radius);
            }
        }
    }
    return total_vol;
}

inline double dichotomy(double target, vector<hole> const & holes, double const start, double const end){
    double middle = start + (end - start) / 2;
    double value = cheeze_amount(middle, holes);

    // Precision should be 1e-6 but 1e-3 passes
    // Or maybe its meant to be 1e-3 microm => 1e-6 mm
    if (abs(value - target) < 1e-3)
        return middle;
    if (value > target){
        return dichotomy(target, holes, start, middle);
    }else{
        return dichotomy(target, holes, middle, end);
    }
}

inline void do_search(int const slices, vector<hole> const & holes){
    double total_cheese = cheeze_amount(100.0, holes);

    // Position to start
    double start_pos = 0.0;

    for (int i = 1; i != slices; i++){
        // Dichotomy research
        double result = dichotomy(i * total_cheese / (double)slices, holes, start_pos, 100.0);
        printf("%lf\n", (result - start_pos));

        start_pos = result;
    }
    printf("%lf\n", (100.0 - start_pos));
    return;
}

int main()
{
    // Holes in cheese
    // 0<<10000
    int hole_amount;

    // 1<<100
    int slices;

    while (scanf("%d %d", &hole_amount, &slices) == 2){
        if (hole_amount != 0){
            vector<hole> holes;
            // Read holes
            for (int i = 0; i != hole_amount; i++){
                holes.push_back(hole());
                int x, y, z, r;
                scanf("%d %d %d %d", &r, &x, &y, &z);
                holes[i].radius = (double) r / 1000.0;
                holes[i].pos = (double) z / 1000.0;
                holes[i].volume = spherical_cap(holes[i].radius, 2 * holes[i].radius);
            }

            // Search for solution
            do_search(slices, holes);
        } else {
            for (int i = 0; i != slices; i++)
                printf("%lf\n", 100.0/(double)slices);
        }
    }
    return 0;
}