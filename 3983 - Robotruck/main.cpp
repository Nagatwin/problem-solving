#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct vector_2d {
    int x;
    int y;

    vector_2d (int x_, int y_) : x(x_), y(y_) {}

    inline int dist (const vector_2d & other) const{
        return abs(x - other.x) + abs(y - other.y);
    }
};

struct package : vector_2d{
    int weight;

    package (int x_, int y_, int w_) : vector_2d (x_, y_), weight(w_) {}
};

struct parameters{
    vector<int> & distances_packages;
    vector<int> & distances_loader;
    vector<package> & packages;
    vector<int> min_dist;
    int max_weight;

    parameters(vector<int> & distances_packages_, vector<int> & distances_loader_, vector<package> & packages_, int max_weight_) : 
        distances_packages(distances_packages_),
        distances_loader(distances_loader_),
        packages(packages_),
        max_weight(max_weight_) {}
};

// Distance of a trip starting from star_packet to end_packet
inline int dist_of_trip(parameters * param, int start_packet, int end_packet){
    // To start and from end dists
    int dist = param->distances_loader[start_packet] + param->distances_loader[end_packet];

    // Travel dists
    for (int i = start_packet; i < end_packet; i++)
        dist += param->distances_packages[i];

    // Previous min
    if (start_packet != 0)
        dist += param->min_dist[start_packet - 1];
    return dist;
}

// Computes the min distance to handle the last_packet first packets
inline void min_dist_div(parameters * param, int const last_packet){
    // Dist if doing a trip for unly 1 packet
    int min_dist_found = dist_of_trip(param, last_packet, last_packet);

    // Init loop
    int first_packet = last_packet - 1;
    int expected_weight = param->packages[last_packet].weight;
    
    while (first_packet >= 0){
        // Add weight and check if it fits
        expected_weight += param->packages[first_packet].weight;
        if(expected_weight > param->max_weight)
            break;
            
        // Compute new distance
        int distance = dist_of_trip(param, first_packet, last_packet);
        min_dist_found = min(min_dist_found, distance);

        --first_packet;
    }
    param->min_dist.push_back(min_dist_found);
}

// Computes dynamically the dist
inline int min_dist(parameters * param){
    for (unsigned int i = 0; i < param->packages.size(); i++)
        min_dist_div(param, i);
    return param->min_dist.back();
}

int main()
{
    int cases;
    scanf("%d", &cases);

    for (int cas = 0; cas < cases; cas++){
        // robot capacity
        // <<100
        int C;
        scanf("%d", &C);

        // number of packages
        // << 10000
        int N;
        scanf("%d", &N);

        vector<package> packages;
        for (int package = 0; package < N; package++){
            int x, y, w;
            scanf("%d %d %d", &x, &y, &w);
            packages.emplace_back(x, y, w);
        }

        // distance calculations
        vector_2d loader(0, 0);
        vector<int> distances_packages;
        vector<int> distances_loader;
        for (int src = 0; src < N - 1; src++){
            distances_packages.push_back(packages[src].dist(packages[src+1]));
            distances_loader.push_back(packages[src].dist(loader));
        }
        distances_loader.push_back(packages[N - 1].dist(loader));

        // Look for a solution
        parameters s = parameters(distances_packages, distances_loader, packages, C);

        // Print
        cout << min_dist(&s) << endl;

        // (╯°□°）╯︵ ┻━┻
        if (cas != cases - 1)
            cout << endl;
    }
    return 0;
}