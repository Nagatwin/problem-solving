#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <string>

using namespace std;

typedef map<char, string> translations_map;

int main(){
    // m translation letters, n pairs
    // 1<m<500
    // 1<n<50
    int m, n;
    while (scanf("%d %d", &m, &n) == 2){
        // Read translations
        char c1, c2;
        translations_map translations;
        translations_map::iterator it;

        for (int i = 0; i < m; i++){
            // read next translation
            cin >> c1;
            cin >> c2;

            string s2 = string(1,c2);

            // build string of dest
            it = translations.find(c2);
            if (it != translations.end()){
                s2.append(translations[c2]);
            }

            // try to find the letter
            it = translations.find(c1);
            if (it == translations.end())
                // We don't have this letter in, add it
                translations[c1] = s2;
            else
                // add missing letters
                for(char& c : s2) 
                    if (translations[c1].find(c)==string::npos)
                        translations[c1].append(string(1, c));

            // Update all letters having this one
            // For item in map
            for(it = translations.begin(); it != translations.end(); it++)
                if ((it->second).find(c1)!=string::npos)
                    for(char& c : translations[c1])
                        if ((it->second).find(c)==string::npos)
                            (it->second).append(string(1, c));
        }

        // Read translations
        for (int i = 0; i < n; i++){
            string s1;
            string s2;

            cin >> s1;
            cin >> s2;

            if (s1.length() != s2.length())
                cout << "no" << endl;
            else{
                bool ok = true;
                for (unsigned int k = 0; k < s1.length(); k++){
                    char c1 = s1.at(k);
                    char c2 = s2.at(k);

                    // different chars
                    if (c1 != c2){
                        // If not in map, false
                        if(translations.find(c1) == translations.end() ||
                            translations[c1].find(c2) == string::npos ){
                            ok = false;
                            break;
                        }
                    }
                }
                if (ok) {
                    cout << "yes" << endl;
                } else {
                    cout << "no" << endl;
                }
            }
        }
    }
    return 0;
}