#include <limits>
#include <iostream>
#include <vector>
#include <queue>

using namespace std;

// HopcroftKarp Algo taken & FIXED from course

// Artificial node (unused otherwise) -- end of augmenting path
#define NIL 0
// "Infinity", i.e., value larger than min(|X|, |Y|)
#define INF (numeric_limits<unsigned int>::max)()

#define MAXX 101
#define MAXY 101

// Neighbors in Y of nodes in X
vector<int> Adj[MAXX];

// Matching X-Y and Y-X
int PairX[MAXX];
int PairY[MAXY];

// Augmenting path lengths
unsigned int Dist[MAXX];

inline bool DFS(int const x) {
    if (x == NIL)
        return true; // reached NIL
    for (int const y : Adj[x])
        if (Dist[PairY[y]] == Dist[x] + 1 && DFS(PairY[y])) { // follow trace of BFS
            PairX[x] = y; // add edge from x to y to matching
            PairY[y] = x;
            return true;
        }
    Dist[x] = INF;
    return false; // no augmenting path found
}

inline bool BFS(vector<int> const & X) {
    queue<int> Q;
    Dist[NIL] = INF;
    for(int const x : X) { // start from nodes that are not yet matched
        Dist[x] = (PairX[x] == NIL) ? 0 : INF;
        if (PairX[x] == NIL)
            Q.push(x);
    }

    while (!Q.empty()) { // find all shortest paths to NIL
        int x = Q.front(); Q.pop();
        if (Dist[x] < Dist[NIL]) // can this become a shorter path?
            for (auto y : Adj[x])
                if (Dist[PairY[y]] == INF) {
                    Dist[PairY[y]] = Dist[x] + 1; // update path length
                    Q.push(PairY[y]);
                }
    }
    return Dist[NIL] != INF; // any shortest path to NIL found?
}

inline int HopcroftKarp(vector<int> const & X, vector<int> const & Y) {
    // Reset pairs
    for (int const x : X)
        PairX[x] = NIL;
    for (int const y : Y)
        PairY[y] = NIL;

    int Matching = 0; // count number of edges in matching
    while (BFS(X)) { // find all shortest augmenting paths
        for(auto x : X) // update matching cardinality
            if (PairX[x] == NIL && // node not yet in matching?
                DFS(x)) // does an augmenting path start at x?
                Matching++;
    }
    return Matching;
}

int main()
{
    // r rows, c columns
    int r, c;

    while (scanf("%d %d", &r, &c) == 2){
        // read inputs
        vector<vector<bool>> top_camera(r, vector<bool>(c, true));

        vector<long long> front_camera(c, -1);
        vector<long long> side_camera(r, -1);

        long long crates_left = 0;

        // Get cameras
        long long val;
        for (int row = 0; row < r; row++){
            for (int col = 0; col < c; col++){
                scanf("%lld", &val);
                top_camera[row][col] = val != 0;
                front_camera[col] = max(front_camera[col], val);
                side_camera[row] = max(side_camera[row], val);
                
                // We need to leave 1 crate for top camera
                // And there are val
                crates_left += val - top_camera[row][col];
            }
        }
        
        // Reset adjacency list
        for (int x = 0; x < MAXX; x++)
            Adj[x].clear();
        
        // Connected if same height from cameras
        for (int x = 0; x < c; x++)
            for (int y = 0; y < r; y++)
                if (front_camera[x] == side_camera[y] && top_camera[y][x])
                    Adj[x+1].push_back(y + 1);

        // X set is the set of columns indexes for nodes
        vector<int> X;
        for (int x = 1; x <= c; x++)
            if (front_camera[x - 1])
                // We must leave a pile there
                X.push_back(x);
        
        // Y set is the set of rows indexes for nodes
        vector<int> Y;
        for (int y = 1; y <= r; y++)
            if (side_camera[y - 1])
                Y.push_back(y);
        
        // Find Max Cardinality Bipartite Matching
        HopcroftKarp(X, Y);
        
        // Update crates count
        for (int x : X)
            // Add a pile for every column
            crates_left -= front_camera[x - 1] - 1;
        
        for (int y : Y)
            // Without pairs, we have to leave one more pile for rows
            if (PairY[y] == NIL)
                crates_left -= side_camera[y - 1] - 1;
        
        // Out
        cout << crates_left << endl;
    }
    return 0;
}