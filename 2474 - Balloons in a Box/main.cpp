#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

// Const
const double PI = acos(-1);

// Base struct
struct vector_3d {
    int x;
    int y;
    int z;

    vector_3d() : x(0), y(0), z(0) {}
    vector_3d (int x_, int y_, int z_) : x(x_), y(y_), z(z_) {}

    inline double dist_eucl (const vector_3d & other) const{
        return sqrt((double)(pow(x - other.x, 2) + pow(y - other.y, 2) + pow(z - other.z, 2))) ;
    }
};

// Base sphere struct
struct sphere : vector_3d {
    double radius;

    // Closest border
    double bmin;

    sphere() : vector_3d(), radius(0), bmin(0) {}
};

inline void dfs_glouton(vector<bool> & toggle, vector<sphere> & points, double & borne, unsigned int depth = 0, double current_volume = 0){
    // We inflated all bloons
    if (depth == points.size()){
        if (borne < current_volume)
            borne = current_volume;
        return;
    }
    
    for (unsigned int current = 0; current < points.size(); current++){
        // If not yet inflated
        if (!toggle[current]){
            toggle[current] = true;
            // We have not visited this bloon yet, let's dfs it
            // compare to borders, precomputed
            vector<double> values;
            values.push_back(points[current].bmin);

            // compare to bloons
            for (unsigned int i = 0; i < points.size(); i++)
                if (toggle[i] && i != current && points[i].radius > 0)
                    values.push_back(points[current].dist_eucl(points[i]) - points[i].radius);

            double radius = *min_element(values.begin(), values.end());

            // Negative radius = inside other bloon
            if (radius < 0)
                radius = 0;
            
            points[current].radius = radius;

            // compute new volume, avoid PI*4/3 factor to speed up calc
            double new_volume = current_volume + pow(radius, 3);

            // dfs it
            dfs_glouton(toggle, points, borne, depth + 1, new_volume);

            // Reset state for dfs
            toggle[current] = false;
            points[current].radius = 0;
        }
    }
    return;
}

int main()
{
    // Current case
    int cas = 1;

    // Amount of points
    int points_nb;
    scanf("%d", &points_nb);

    while (points_nb != 0){
        // We take in account only points inside box
        int inside_points_nb = points_nb;

        // Read parameters
        vector_3d box_1;
        scanf("%d %d %d", &box_1.x, &box_1.y, &box_1.z);
        vector_3d box_2;
        scanf("%d %d %d", &box_2.x, &box_2.y, &box_2.z);

        // Reorder box coords
        vector_3d lower_box;
        lower_box.x = min(box_1.x, box_2.x);
        lower_box.y = min(box_1.y, box_2.y);
        lower_box.z = min(box_1.z, box_2.z);

        vector_3d higher_box;
        higher_box.x = max(box_1.x, box_2.x);
        higher_box.y = max(box_1.y, box_2.y);
        higher_box.z = max(box_1.z, box_2.z);

        vector<vector_3d> box;
        box.push_back(lower_box);
        box.push_back(higher_box);

        // Read points
        vector<sphere> points;
        for (int i = 0; i < points_nb; i++){
            sphere b;
            scanf("%d %d %d", &b.x, &b.y, &b.z);
            
            // Check if inside
            if (b.x > lower_box.x &&
                b.y > lower_box.y &&
                b.z > lower_box.z &&
                b.x < higher_box.x &&
                b.y < higher_box.y &&
                b.z < higher_box.z){
                    
                // Get closest border
                vector<double> values;
                for (vector_3d & border : box){
                    values.push_back(abs(b.x - border.x));
                    values.push_back(abs(b.y - border.y));
                    values.push_back(abs(b.z - border.z));
                }
                b.bmin = *min_element(values.begin(), values.end());
                points.push_back(b);
            }else{
                inside_points_nb--;
            }
        }
        // Now do the job
        // DFS flag
        vector<bool> toggle(inside_points_nb, false);
        double borne = 0;

        // Dfs
        dfs_glouton(toggle, points, borne);

        // Compute free volume
        double volume_occ = PI * borne * 4.0/3.0;
        double volume_free =
            (double)(abs(box_1.x - box_2.x) *
            abs(box_1.y - box_2.y) *
            abs(box_1.z - box_2.z)) -
            volume_occ;

        cout << "Box " << cas << ": " << int(round(volume_free)) << endl << endl;
        cas ++;
        scanf("%d", &points_nb);
    }
    return 0;
}