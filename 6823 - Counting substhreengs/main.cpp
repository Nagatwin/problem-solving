#include <iostream>
#include <cstring>

#define RESET(tab) do{tab[0] = 1; tab[1] = 0; tab[2] = 0;} while(0)

using namespace std;

// This code basically behaves like an automate
// We have 3 states for every modulo 3 leftover
// For every state, we keep in memory how many occurences we have to add if we reach this state
// This works because we can count the mod 3 leftover easily (sum of digits thx to the 3 div)
//
// The states_count[i] basically stores how many previous starts could be completed (and divisible by 3)
// If we reach a leftover of i
// It equals the number of substrings starting at the first digit that are of leftover i
int main() {
    string str;

    while (getline(cin, str)) {
        // Count for each state
        // We start at 1 for state 0 because the first mod 3 char we find gives us 1 sub
        // We start at 0 for others
        long long states_count[3];
        long long occurences = 0;
        int state = 0;
        RESET(states_count);
        
        for (unsigned int i = 0; i < str.length(); i++){
            if (isdigit(str[i])){
                // x is div by 3 iif sum(digits(x)) div by 3
                // Compute next state
                state = (state + int(str[i]) - 48) % 3;

                // Add the correcponding count
                occurences += states_count[state];

                // Increase the count
                states_count[state]++;
            } else {
                RESET(states_count);
                state = 0;
            }
        }
        cout << occurences << endl;
    }
    return 0;
}