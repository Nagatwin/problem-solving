#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

struct vect2{
    int x, y;
    vect2(int x_, int y_) : x(x_), y(y_) {}

    inline double vprod(vect2 const & other) const{
        return double(y * other.x - x * other.y);
    }
};

int main(){
    // n length vertices
    int n;

    while(scanf("%d", &n) == 1) {
        vector<vect2> points;

        int x, y;

        int base_min_x = 2000, base_max_x = -2000;
        for (int i = 0; i < n; i++){
            scanf("%d %d", &x, &y);
            points.emplace_back(x, y);

            if (y == 0){
                base_min_x = min(x, base_min_x);
                base_max_x = max(x, base_max_x);
            }
        }

        points.push_back(points[0]);

        // compute area and centroid
        double area = 0, centroid_x = 0;
        for (int i = 0; i < n; i++) {
            double vp = double(points[i].vprod(points[i+1]));
            
            area += vp;
            centroid_x += vp * double(points[i].x + points[i+1].x);
        }

        // flip if we turned the wrong way around
        if (area < 0){
            area *= -1;
            centroid_x *= -1;
        }

        area = area / 2;
        centroid_x /= (6 * area);

        if (points[0].x >= base_max_x){
            // We attach at a point outside base on left side
            if (centroid_x > base_max_x)
                cout << "unstable" << endl;
            else{
                // Min weight
                double m_min = area * (base_min_x - centroid_x) / (points[0].x - base_min_x);
                // Can not be negative
                m_min = max(0.0, m_min);
                cout << int(floor(m_min)) << " .. ";

                if (points[0].x != base_max_x){
                    // Max weight
                    double m_max = area * (base_max_x - centroid_x) / (points[0].x - base_max_x);
                    cout << int(ceil(m_max));
                } else 
                    cout << "inf";
                
                cout << endl;
            }
        } else if (points[0].x <= base_min_x){
            // We attach at a point outside base on right side
            if (centroid_x < base_min_x)
                cout << "unstable" << endl;
            else {
                // Min weight
                double m_min = area * (base_max_x - centroid_x) / (points[0].x - base_max_x);
                // Can not be negative
                m_min = max(0.0, m_min);
                cout << int(floor(m_min)) << " .. ";

                if ((points[0].x - base_min_x) != 0) {
                    // Max weight
                    double m_max = area * (base_min_x - centroid_x) / (points[0].x - base_min_x);
                    cout << int(ceil(m_max));
                } else 
                    cout << "inf";
                
                cout << endl;
            }
        } else {
            // we attach in the basis
            cout << "0 .. inf" << endl;
        }
    }
    return 0;
}