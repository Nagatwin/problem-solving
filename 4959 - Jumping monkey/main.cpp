#include <iostream>
#include <queue>
#include <stack>

using namespace std;

#define NMAX 21

// Adjacency matrix
uint32_t adjacency[NMAX];

// Distance of that state to the next one
char dists[(1<<NMAX)];

// Parent of the said state in bfs
uint32_t parent[(1<<NMAX)];

// The coup that has been used to get this state
uint32_t coup[(1<<NMAX)];

inline int BFS(char const n) {
    queue<uint32_t> Q;
    uint32_t start_state = (1<<n) - 1;
    Q.push(start_state);
    dists[start_state] = 0;

    while (!Q.empty()) {
        uint32_t x = Q.front(); Q.pop();

        if (!x)
            return x;
        
        // On a une solution
        else {
            char next_dist = dists[x] + 1;
            // Pour tout les shoots
            for (char i = 0; i < n; i++) {
                // On calcule l'état modifié de ce coup
                uint32_t modified_state = x & (~(1<<i));

                // On calcule l'état d'arrivée
                uint32_t next_state = 0;
                for (uint32_t arb = 0; arb < NMAX; arb++)
                    if (modified_state & (1<<arb))
                        next_state |= adjacency[arb];
                
                // Si on a pas déjà exploré ce noeud, on l'ajoute
                if (!dists[next_state]){
                    parent[next_state] = x;
                    coup[next_state] = i;
                    dists[next_state] = next_dist;
                    Q.push(next_state);
                }
            }
        }
    }
    return -1;
}

int main()
{
    // n trees, m edges
    int n, m;
    scanf("%d %d", &n, &m);

    while (n != 0){
        // Reset matrix
        for (int i = 0; i < NMAX; i++)
            for (int j = 0; j < NMAX; j++)
                adjacency[i] = 0;
        
        // Reset parent & dists
        for (int i = 0; i < (1<<NMAX); i++)
            parent[i] = 0;
        for (int i = 0; i < (1<<NMAX); i++)
            dists[i] = 0;

        // Read matrix
        int e, f;
        for (int i = 0; i < m; i++){
            scanf("%d %d", &e, &f);
            adjacency[e] |= (1<<f);
            adjacency[f] |= (1<<e);
        }

        // Compute solution
        int solution = BFS(n);
        if (solution == -1)
            cout << "Impossible" << endl;
        else {
            // Unpack solution
            stack<uint32_t> s;

            uint32_t current = solution;
            // Backtrack
            for (char i = 0; i < dists[solution]; i++){
                s.push(coup[current]);
                current = parent[current];
            }

            // Display
            cout << int(dists[solution]) << ":";
            for (char i = 0; i < dists[solution]; i++){
                cout << " " << s.top(); s.pop();
            }
            cout << endl;
        }

        scanf("%d %d", &n, &m);
    }
    return 0;
}