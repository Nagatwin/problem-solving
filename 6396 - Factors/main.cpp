#include <iostream>
#include <map>

using namespace std;

#define MAXK (uint64_t(1)<<63)

unsigned int const primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107};
unsigned int factors[100];

map<uint64_t, uint64_t> m;

inline void generate (unsigned int const current_p, unsigned int const current_pos, __int128 u, __int128 const permu, __int128 const sum){
    // add permutation if better
    if (m.find(permu) == m.end() || m[permu] > u)
        m[permu] = u;

    // to cumpute next permutation
    __int128 next_permu = permu;

    for (unsigned int times = 1; times <= factors[current_pos - 1]; times++){
        // Mult by the current prime
        u *= current_p;

        // Stop if too big compared to pb data
        if (u >= (MAXK))
            break;
        
        factors[current_pos]++;

        // Compute next permutation
        next_permu *= (sum + times);
        next_permu /= times;
        
        // Stop if too big compared to pb data
        if (next_permu >= (MAXK))
            break;

        // Generate recursively
        generate(primes[(current_pos+1)], current_pos+1, u, next_permu, sum + times);
    }
}

inline void generate_all(){
    __int128 u = 1;
    for (unsigned int times = 1; u < (MAXK); times++){
        u *= 2;
        factors[0]++;
        generate(3, 1, u, 1, times);
    }
}

int main(){
    uint64_t n;

    generate_all();
    
    while(cin>>n) {
        cout << n << " " << m[n] << endl;
    }

    return 0;
}