#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <map>

using namespace std;

#define MAXK (uint64_t(1)<<63)

vector<unsigned int> primes;

inline bool is_prime(unsigned int to_test){
    for (unsigned int u : primes)
        if (to_test % u == 0)
            return false;
    return true;
}

inline unsigned int get_prime(unsigned int pos){
    while (pos >= primes.size()){
        unsigned int last = primes.back() + 2;
        while (!is_prime(last))
            last += 2;
        primes.push_back(last);
    }
    return primes[pos];
}

map<uint64_t, uint64_t> m;

void generate (vector<unsigned int> & factors, unsigned int current_p, unsigned int current_pos, __int128 u, __int128 permu, __int128 sum){
    if(current_pos >= factors.size())
        factors.push_back(0);
    // compute permu
    if (m.find(permu) == m.end() || m[permu] > u)
        m[permu] = u;
    __int128 next_permu = permu;

    for (unsigned int times = 1; times <= factors[current_pos - 1]; times++){
        u *= current_p;
        if (u >= (MAXK))
            break;
        factors[current_pos] += 1;
        next_permu *= (sum + times);
        next_permu /= times;
        
        if (next_permu >= (MAXK))
            break;

        generate(factors, get_prime(current_pos+1), current_pos+1, u, next_permu, sum + times);
    }
}

void generate_all(){
    __int128 u = 1;
    vector<unsigned int> v;
    v.push_back(0);
    for (unsigned int times = 1; u < (MAXK); times++){
        u *= 2;
        v[0]++;
        generate(v, 3, 1, u, 1, times);
    }
}

int main(){
    // n length vertices
    uint64_t n;

    primes.push_back(2);
    primes.push_back(3);

    generate_all();
    
    while(cin>>n) {
        cout << n << " " << m[n] << endl;
    }

    return 0;
}