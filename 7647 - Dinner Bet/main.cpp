#include <iostream>
#include <vector>

#define SIZE 11
#define BINOM_SIZE 51

using namespace std;

unsigned long long binom[BINOM_SIZE][BINOM_SIZE];
double expectancy[SIZE][SIZE][SIZE];
double seen[SIZE][SIZE][SIZE];

// All pb const
inline double compute_expectancy(unsigned int const N, unsigned int const D, unsigned int const C, unsigned int const Pmax, unsigned int const Imax,
    // I : amount of common cards to tick
    // P1 : amount of cards from player 1 to tick
    // P2 : amount of cards from player 2 to tick
    unsigned int const I, unsigned int const P1, unsigned int const P2){
    // If we already went on this cell, send computed result
    if (seen[I][P1][P2])
        return expectancy[I][P1][P2];
    
    // We have now checked this cell
    seen[I][P1][P2] = true;
    
    // p000 will be assigned once
    double p000, previous_expectancy = 0;

    // For every situation having less card left
    for (unsigned int p1 = 0; p1 <= P1; p1++)
        for (unsigned int p2 = 0; p2 <= P2; p2++)
            for (unsigned int i = 0; i <= I; i++){
                // For this situation change, we picked
                unsigned int i_picked = I - i;
                unsigned int p1_picked = P1 - p1;
                unsigned int p2_picked = P2 - p2;
                int no_picked = D - i_picked - p1_picked - p2_picked;

                // Filter impossible picks
                if (i_picked + p1_picked + p2_picked > D)
                    continue;

                // Compute conditionnal prob
                double cond_proba = double(binom[i_picked][I] * binom[p1_picked][P1] * binom[p2_picked][P2] * binom[no_picked][N - I - P1 - P2])/double(binom[D][N]);
                
                // This one corresponds to the pb to stay in the current situation
                if (i == I && p1 == P1 && p2 == P2)
                    p000 = cond_proba;
                // Add up all expectancies of previous situations (this is a weighted average)
                else
                    previous_expectancy += cond_proba * compute_expectancy(N, D, C, Pmax, Imax, i, p1, p2);
            }
    
    // +1 expectancy in average
    // Add up staying in same cell indefinately
    return expectancy[I][P1][P2] = (previous_expectancy + 1) / (1.0 - p000);
}

int main(){
    // generating binomial coeff
    for (int i = 0; i < BINOM_SIZE; i++)
        for (int j = 0; j < BINOM_SIZE; j++)
            binom[i][j] = 0;
    
    for (int i = 0; i < BINOM_SIZE; i++)
        binom[0][i] = 1;
    
    for (int n = 1; n < BINOM_SIZE; n++)
        for (int k = 1; k < BINOM_SIZE; k++)
            binom[k][n] = binom[k-1][n-1] + binom[k][n-1];
    
    // N 1<<50
    // D 1<<min(10,N)
    // C 1<<min(10,N)
    int N, D, C;
    while(scanf("%d %d %d", &N, &D, &C) == 3) {
        // Reset tables (will be compiled as memset)
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                for (int k = 0; k < SIZE; k++)
                    expectancy[i][j][k] = 0;
                    
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                for (int k = 0; k < SIZE; k++)
                    seen[i][j][k] = false;
                
        
        int c;
        // cards for each player
        vector<bool> card_p1(N+1, false);
        vector<bool> card_p2(N+1, false);

        // player 1
        for (int i = 0; i < C; i++){
            scanf("%d", &c);
            card_p1[c] = true;
        }

        // player 2
        for (int i = 0; i < C; i++){
            scanf("%d", &c);
            card_p2[c] = true;
        }

        // P1max == P2max
        // P1max + Imax = C
        unsigned int P1max = 0, P2max = 0, Imax = 0;

        // Imax : nb of cards P1 & P2 have
        // P1max (P2max) : nb of cards P1 have and not P2
        for (int i = 0; i <= N; i++){
            if (card_p1[i] && card_p2[i])
                Imax++;
            else if (card_p1[i])
                P1max++;
            else if (card_p2[i])
                P2max++;
        }

        // End situation have 0 expectancy to win
        seen[0][0][0] = true;
        for (int i = 1; i <= P1max; i++)
            seen[0][i][0] = true;
        for (int i = 1; i <= P2max; i++)
            seen[0][0][i] = true;

        // Compute end expectancy from a situation with P1max Imax and P2max cards un ticked
        printf("%.5f\n", compute_expectancy(N, D, C, P1max, Imax, Imax, P1max, P2max));
    }
    return 0;
}