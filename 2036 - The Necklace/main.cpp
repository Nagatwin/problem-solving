#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>

using namespace std;

#define rm(v,a) do{const vector<int>::iterator it = std::find(v.begin(), v.end(), a);v.erase(it, it + 1);} while(0)

#define MAXN 50

// From course
vector<int> Adj[MAXN];
vector<int> Circuit;

// Here we assume that we have nodes without odd degree
inline void Hierholzer(int const start) {
    Circuit.clear();
    int v = start;
    
    stack<int> Stack;
    Stack.push(v);
    while (!Stack.empty()) {
        if (!Adj[v].empty()) { // follow edges until stuck
            Stack.push(v);
            int tmp = *Adj[v].begin();
            rm(Adj[v],tmp); // remove edge, modifying graph
            rm(Adj[tmp],v);
            v = tmp;
        } else { // got stuck: stack contains a circuit
            Circuit.push_back(v); // append node at the end of circuit
            v = Stack.top(); // backtrack using stack, find larger circuit
            Stack.pop();
        }
    }
}

int main()
{
    // T cases
    int T;
    scanf("%d", &T);

    for(int cas = 1; cas <= T; cas++){
        // N beads
        int N;
        scanf("%d", &N);
        
        // Clear graph
        for (int i = 0; i <= 50; i++)
            Adj[i].clear();

        // Read beads
        int c1, c2;
        for (int i = 0; i < N; i++){
            scanf("%d %d", &c1, &c2);

            Adj[c1].push_back(c2);
            Adj[c2].push_back(c1);
        }

        // scan degrees
        bool has_odd_vertex = false;
        for (unsigned int i = 0; i <= 50; i++){
            if (Adj[i].size() % 2 == 1){
                has_odd_vertex = true;
                break;
            }
        }

        cout << "Case #" << cas << endl;
        if (!has_odd_vertex){
            // look for an eulerian cycle
            Hierholzer(c1);
            for (unsigned int i = 0; i < Circuit.size() - 1; i++)
                cout << Circuit[i] << " " << Circuit[i+1] << endl;
        } else
            cout << "some beads may be lost" << endl;
        cout << endl;
    }
    return 0;
}