#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// Main algo
inline void dfs(vector<int> const & diffs, int const k, int & borne, int const machines_left, int const current_battery = 0, int const jockers_left = 0, int const current_diff = 0){
    // Check if we are on a leaf
    if (machines_left == 0){
        if (borne > current_diff)
            borne = current_diff;
        return;
    }
    
    // Test every other possible combinason
    for (int i = current_battery; i < current_battery + jockers_left + 1; i++){
        if (diffs[i] < borne){
            dfs(diffs, k, borne,
            // We stuffed 1 more machine
            machines_left - 1,
            // We took the battery i and i + 1
            i + 2,
            // We have 2k - 2 jockers available, minus the ones we used
            jockers_left + (2 * k - 2) - (i - current_battery),
            // The battery difference for the additional machine is taken into account
            max(current_diff, diffs[i]));
        }
    }
    return;
}

int main()
{
    // n machines, k batteries
    int n, k;
    while (scanf("%d %d", &n, &k) == 2){
        // Read weights
        vector<int> weights;
        int pi;
        for (int i = 0; i < 2 * n * k; i++){
            scanf("%d", &pi);
            weights.push_back(pi);
        }

        // Sort vector
        sort(weights.begin(), weights.end());

        // Compute diffs and starting borne
        vector<int> dists;
        int borne = 0;
        for (int i = 0; i < 2 * n * k - 1; i++){
            dists.push_back(weights[i+1] - weights[i]);
            if (borne < dists[i])
                borne = dists[i];
        }

        // Do the job :o
        dfs(dists, k, borne, n, 0, 0, 0);

        cout << borne << endl;
    }
    return 0;
}