#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

inline bool handle(int const amount, float const width, float const size)
{
    vector<float> v(amount);
    bool result = true;
    
    // Read inputs
    for (int i = 0; i < amount; i++)
    {
        scanf("%f", &v[i]);
    }

    // Sort vector
    sort(v.begin(), v.end());

    // Check first step
    if (2 * v[0] > width)
        result = false;

    // Check each step
    for (int i = 1; i < amount; i++)
    {
        if (v[i] - v[i - 1] > width)
            result = false;
    }

    // Check last step
    if (2 * (size - v[amount - 1]) > width)
        result = false;

    return result;
}

int main()
{
    // 0<<1000
    int nx, ny;

    // 0<<50
    float width;
    while (scanf("%d %d %f", &nx, &ny, &width) == 3)
    {
        if (nx == 0 && ny == 0 && width == 0.0)
            break;
        
        bool result = handle(nx, width, 75.0);
        result &= handle(ny, width, 100.0);
        
        if (result)
            printf("YES\n");
        else
            printf("NO\n");
    }
    return 0;
}