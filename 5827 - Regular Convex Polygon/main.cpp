#include <iostream>
#include <cmath>

using namespace std;

// lowest exp that gets accepted
#define EPS 1e-5

// Const
const long double PI = acos(-1);

struct vect2{
    long double x, y;
    vect2(long double x_, long double y_) : x(x_), y(y_) {}

    inline long double vprod(vect2 const & other) const{
        return x * other.y - y * other.x;
    }

    vect2 operator-(const vect2& other){
        return vect2(x - other.x, y - other.y);
    }

    inline long double angle () const{
        return atan2(y, x);
    }

    inline long double scal(const vect2& other) const{
        return x * other.x + y * other.y;
    }

    inline long double norm() const{
        return hypot(x, y);
    }

    inline long double sq_norm() const{
        return x * x + y * y;
    }
};

int main(){
    double x, y;
    scanf("%lf %lf", &x, &y);
    
    do {
        vect2 p1(x, y);
        scanf("%lf %lf", &x, &y);
        vect2 p2(x, y);
        scanf("%lf %lf", &x, &y);
        vect2 p3(x, y);

        vect2 v1 = p1 - p2;
        vect2 v2 = p3 - p2;
        vect2 v3 = p1 - p3;

        long double a1, a2, a3;
        // t= 0.29 0.35 0.32
        //a1 = fabs(v2.angle() - v1.angle());
        //a2 = fabs(v3.angle() - v2.angle());
        //a3 = fabs(v1.angle() - v2.angle());

        // t = 0.32 0.32 0.35
        //a1 = fabs(atan2(v1.vprod(v2), v1.scal(v2)));
        //a2 = fabs(atan2(v2.vprod(v3), v2.scal(v3)));
        //a3 = fabs(atan2(v3.vprod(v1), v3.scal(v1)));

        // t 0.33 0.32 0.33
        //a1 = acos((v2.sq_norm() + v3.sq_norm() - v1.sq_norm()) / (2. * v3.norm() * v2.norm()));
        //a2 = acos((v3.sq_norm() + v1.sq_norm() - v2.sq_norm()) / (2. * v1.norm() * v3.norm()));
        //a3 = acos((v1.sq_norm() + v2.sq_norm() - v3.sq_norm()) / (2. * v2.norm() * v1.norm()));
        
        // t = 0.32 0.26 0.32
        // unoriented angle
        a1 = acos(v1.scal(v2)/(v1.norm() * v2.norm()));
        a2 = acos(v2.scal(v3)/(v2.norm() * v3.norm()));
        //a3 = acos(v3.scal(v1)/(v3.norm() * v1.norm()));

        for (int i = 3; i < 1001; i++){
            long double ref = PI / (long double)(i);
            if ((fmod(a1, ref) < EPS || ref - fmod(a1, ref) < EPS)
                && (fmod(a2, ref) < EPS || ref - fmod(a2, ref) < EPS)
                // Not needeed
                //&& (fmod(a3, ref) < EPS || ref - fmod(a3, ref) < EPS)
                ){
                    printf("%d\n", i);
                    break;
                }
        }
    } while(scanf("%lf %lf", &x, &y) == 2);
    return 0;
}