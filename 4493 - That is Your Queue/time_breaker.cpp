#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

int main()
{
    // 1<<1000000000
    // Population of the country
    int P;

    // 1<<1000
    // Commands to process
    int C;

    int current_case = 1;

    while (scanf("%d %d", &P, &C) == 2)
    {
        if (P == 0 && C == 0)
            break;

        // list of people passed through
        list<int> l;
        int current_people = 1;

        // Emergency stack
        list<int> emergency_stack;

        printf("Case %d:\n", current_case);
        for (int i = 0; i < C; i++)
        {
            // Operation to process
            char op;
            scanf("\n%c", &op);

            if (op == 'N')
            {
                // Next
                if (emergency_stack.size() != 0){
                    // We have an emergency
                    int out = emergency_stack.front();
                    emergency_stack.remove(out);
                    printf("%d\n", out);
                    l.push_back(out);
                }else{
                    // Let's ensure we have not skipped sbdy
                    while ((find(l.begin(), l.end(), current_people) != l.end())) 
                        current_people ++;

                    // Everybody not passed
                    if (current_people != P + 1){
                        printf("%d\n", current_people);
                        l.push_back(current_people);
                        current_people++;
                    } else {
                        int out = l.front();
                        l.remove(out);
                        printf("%d\n", out);
                        l.push_back(out);
                    }
                }
            }
            else if (op == 'E')
            {
                // Emergency
                int people;
                scanf("%d", &people);
                l.remove(people);
                emergency_stack.remove(people);
                emergency_stack.push_front(people);
            }
        }

        current_case++;
    }
    return 0;
}