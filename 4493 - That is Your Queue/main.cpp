#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

int main()
{
    // 1<<1000000000
    // Population of the country
    int P;

    // 1<<1000
    // Commands to process
    int C;

    int current_case = 1;

    while (scanf("%d %d", &P, &C) == 2)
    {
        if (P == 0 && C == 0)
            break;

        // list of people
        list<int> l;
        for (int i = 1; i <= min(P, C); i++)
            l.push_back(i);
        
        printf("Case %d:\n", current_case);
        for (int i = 0; i < C; i++)
        {
            // Operation to process
            char op;
            scanf("\n%c", &op);

            if (op == 'N')
            {
                int out = l.front();
                l.remove(out);
                printf("%d\n", out);
                l.push_back(out);
            }
            else if (op == 'E')
            {
                // Emergency
                int people;
                scanf("%d", &people);
                l.remove(people);
                l.push_front(people);
            }
        }
        current_case++;
    }
    return 0;
}