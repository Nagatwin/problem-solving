#include <iostream>

using namespace std;

inline uint64_t count(uint64_t n) {
    uint64_t res = 0;
    
    for (int i = 0; i != 63; i++){
        // le bit i repasse a 1 (n>>(i+1)) fois pendant 2^i nombres à chaque fois
        res += (uint64_t(1)<<i)*(n>>(i+1));
    }

    for (int i = 0; i != 64; i++){
        // cb de temps i reste a 1 avant de passer à 0
        res += 
            // Y a-t'il un 1 en i dans n ?
            ((n>>i)&1)
            *
            // il reste a 1 autant de nombres que ce qu'il reste à déduire avant d'arriver à 0
            ((n & (~((~(uint64_t(0))<<i))))+1);
    }
    return res;
}

int main(){
    long long A, B;
    while(scanf("%lld %lld", &A, &B) == 2) {
        cout << (count(B) - count(A-1)) << endl;
    }
    return 0;
}